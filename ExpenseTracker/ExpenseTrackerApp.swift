//
//  ExpenseTrackerApp.swift
//  ExpenseTracker
//
//  Created by Rizky Ian Indiarto on 9/3/22.
//

import SwiftUI

@main
struct ExpenseTrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
