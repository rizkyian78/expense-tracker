//
//  Extensions.swift
//  ExpenseTracker
//
//  Created by Rizky Ian Indiarto on 9/3/22.
//

import Foundation
import SwiftUI

extension Color {
    static let background = Color("Background")
    static let icon = Color("Icon")
    static let text = Color("Text")
    static let uiBackground = Color(uiColor: .systemBackground)
}
